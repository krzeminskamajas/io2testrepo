using NUnit.Framework;
using System;

namespace StringCalculatorr
{
    public class Tests
    {
        [SetUp]
        public void Setup()
        {
        }

        [Test]
        public void EmptyString()
        {
            
            Calculator moj_program = new Calculator();
            Assert.AreEqual( 0, moj_program.Calculate(""));
        }
        [Test]
        public void SingleNUmberReturnsValue()
        {

            Calculator moj_program = new Calculator();
            Assert.AreEqual(256, moj_program.Calculate("256"));
        }
        [Test]
        public void DelimetersofTwoNumbers()
        {

            Calculator moj_program = new Calculator();
            Assert.AreEqual(8, moj_program.Calculate("3,5"));
        }
        [Test]
        public void NewLinrofTwoNumbers()
        {

            Calculator moj_program = new Calculator();
            Assert.AreEqual(8, moj_program.Calculate("3\n5"));
        }
        [Test]
        public void MixofSeparators()
        {

            Calculator moj_program = new Calculator();
            Assert.AreEqual(10, moj_program.Calculate("3,5\n2"));
        }
        [Test]
        public void NegativeNumberWithSeparators()
        {

            Calculator moj_program = new Calculator();
            Assert.Throws<FormatException>(() => moj_program.Calculate("-3,5\n2"));
            
        }
        [Test]
        public void NumbersGreaterThan1000Ingored()
        {

            Calculator moj_program = new Calculator();
            Assert.AreEqual(1, moj_program.Calculate("1,1005"));
            Assert.AreEqual(997, moj_program.Calculate("1,996"));

        }
        [Test]
        public void OwnDelimiter()
        {

            Calculator moj_program = new Calculator();
            Assert.AreEqual(6, moj_program.Calculate("//#1,2#3"));
           

        }
    }
}